const MongoService = require("./mongo.service");
const { v4: uuidv4 } = require('uuid');

class UserService{

    constructor() {
        this.userTable = MongoService.collections.users;
        if(!this.userTable){
            throw new Error("Collection user does not exist!");
        }
    }

    async getAllUsers() {
        return await this.userTable.find({}).toArray();
    }

    async getUsers(surnameFilter) {
        return await this.userTable.find({'surname': new RegExp(surnameFilter, 'i')}).toArray()
    }

    async setUser(user) {
        return await this.userTable.update(
            {
                _id: uuidv4(),
                name: user.name,
                surname: user.surname,
                age: user.age,
                isAdmin: false
            },
            {$set: user},
            {upsert: true}
        );
    }

    async deleteUserByID(userID) {
        return await this.userTable.deleteOne({_id: userID});
    }
}

module.exports = UserService;