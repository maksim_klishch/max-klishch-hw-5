class MainController {
    constructor(app) {
        app.get("/", (req, res) => {
            res.render("index.html", {locals: {title: "Welcome!"}});
        });
    }
}

module.exports = MainController;