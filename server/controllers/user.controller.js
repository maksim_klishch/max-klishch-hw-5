const express = require("express");
const UserService = require("../services/user.service");


class UserController {

    constructor(app) {

        const userRouter = express.Router();

        const users = new UserService();

        userRouter.get("/all", async (req, res) => {
            res.json(await users.getAllUsers());
        })

        userRouter.get("/", async (req, res) => {
            res.json(await users.getUsers(req.query.surnameFilter));
        })

        userRouter.post("/", async (req, res) => {
            try {
                res.json(await users.setUser(req.body));
            } catch(error) {
                console.error(error)
                res.sendStatus(400)
            }
        })

        userRouter.delete("/", async (req, res) => {
            res.json(await users.deleteUserByID(req.body._id));
        })

        app.use("/users", userRouter);
    }
}

module.exports = UserController;