saveUser = updatable(saveUser)
removeUser = updatable(removeUser)

$('#add-user-form').submit(function(event) {
    event.preventDefault();

    saveUser({
        name: this.name.value,
        surname: this.surname.value,
        age: this.age.value
    })

    //clear form
    this.reset()
})

$('#search').change(async () => {
    update()
})

update()


function clearTable()
{
    $("#users-table-body").empty();
}

function displayUsersOnTable(users)
{
    users.forEach((user) => {

        const tableRow = `<tr id="user-row-${user._id}">
            <th>${user._id}</th>
            <td>${user.name}</td>
            <td>${user.surname}</td>
            <td>${user.age}</td>
            <td>${user.isAdmin}</td>
            <td>
                <a>
                    <svg class="btn-delete-user" id="btn-delete-user-${user._id}" xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                    </svg>
                </a>
            </td>
        </tr>`

        $("#users-table-body").append(tableRow);
        $(`#btn-delete-user-${user._id}`).click(() => {
            var result = confirm("Are you sure, than you will delete it?")
            if (!result) return;        

            removeUser(user)
        })

    })
}

async function saveUser(user)
{
    return await fetch("/users", {
        method: "POST",
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify(user)
    })
}

async function getUsers(surnameFilter)
{
    if(!surnameFilter)
    {
        return await fetch(`/users/all`).then(response => response.json())
    }

    return await fetch(`/users?surnameFilter=${surnameFilter}`).then(response => response.json())
}

async function removeUser(user) {
    return await fetch("/users", {
        method: "DELETE",
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify(user)
    }).then(response => response.json())
}

async function update()
{
    const users = await getUsers($('#search').val())
    clearTable()
    displayUsersOnTable(users)
}

function updatable(func)
{
    async function wrapper(...args)
    {
        const res = await func(...args)
        update()
        return res
    }

    return wrapper
}
